#ifndef HEADER_H
#define HEADER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
typedef struct igrac
{
	char ime[20];
	int br;
}IGRAC;
IGRAC igrac;
void menu(void);
int unos(void);
void pitanja(char opcija[200], const char opcija1[200], const char opcija2[200], const char opcija3[200], const char opcija4[200], const char tocno[200], const char kategorija[200], FILE* fp,int* BrojBodova);
void info(void);
void izlaz(void);
void point_to_point(int);
void retry(int);
void tablica_rezultata(void);
void swap(IGRAC*, IGRAC*);
void sort_name(IGRAC*, const int);
void sort_score(IGRAC*, const int);
void output_name(IGRAC*, const int);
void output_score(IGRAC*, const int);
void backup(void);
void player_number(void);
void seek_player(void);
void retry_search(void);

#endif
