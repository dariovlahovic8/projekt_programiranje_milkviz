#define _CRT_SECURE_NO_WARNINGS
#include "header.h"

void menu(void)                                                                                       
{   
	int num;
	int izb = 0;
	do
	{
		printf("Dobro dosli u kviz Milijunasa:\n1. Pokreni kviz\n2. Rezultati\n3. Upute za igranje\n4. Izlaz\n\n");
		scanf("%d", &izb);
		switch (izb){
		
		case 1:
			num = unos();                  
			break;
		case 2:
			tablica_rezultata();
			break;
		case 3:
			info();                                   
			break;
		case 4:
			izlaz();                                 
			break;
		default:
			system("cls");
			while ((getchar()) != '\n');
			printf("Unesite jednu od ponudjenih opcija.\n\n");                 
		
		}
	} while (1);
}
int unos(void) {
	int i, j, k, polje[30], x, temp,score=0;
	int num = 0;
	
	char linija[256];
	FILE* fp;
	FILE* fo;
	char opcija[200], opcija1[200], opcija2[200], opcija3[200], opcija4[200], tocno[200], kategorija[200];
	char spremnik[256][256];
	
	/*char** spremnik = (char**)calloc(256,sizeof(char*));
	for (i = 0; i < 256; i++)
	{
		spremnik[i] = (char*)calloc(256,sizeof(char));
	}*/
	fp = fopen("Univerzalni.txt", "r");
	fo = fopen("NoviSpremnik", "w");
	if (fp == NULL) {
		printf("Nije moguce otvoriti file s pitanjima Univerzalni.txt linija 52");
		exit(1);
	}
	if (fo == NULL) {
		printf("Nije moguce NoviSprmenik stvoriti ili otvoriti linija 55");
		exit(1);
	}
	k = 0;
	for (i = 0; i < 30; i++) {
		polje[i] = 10 * i + 1;// 1 11 21 31 41 51 61 71 81 91 101 111 121 131 141 151 161 171 181 191 201 211 221 231 241 251 261 271 281 291
	}
	for (j = 0; j < 10; j++) {
		srand((unsigned)time(NULL));
		x = rand() % 3;
		temp = (3 * j) + x; //generator nasumicnih brojeva [0,29]
		for (i = polje[3 * j]; i < polje[temp]; i++) { //uzimamo po 11 redova iz datoteke , svako pitanje se sastoji od 11 redova
			fgets(spremnik[k], sizeof(spremnik[k]), fp);//citamo iz datoteke s pitanjima red po red i tako 10 redova i spremamo je kao string char spremink[256][256]
			k++; //k ide od 0 do 9 iliti 10 iteracija k=10
		}
		for (i = 1; i <= 10; i++) { //manipuliram svakom linijom
			fgets(linija, sizeof(linija), fp);//citam red i spremam ga preko pointera u char linija[]
			if (i == 1) {
				printf("\n\n\n\n\n\nPitanje %d.) %s", j + 1, linija); //uvijek na 1. se nalazi pitanje 1, 11, 21, 31...
				fprintf(fo, "Pitanje %d.) %s", j + 1, linija); //salje formatirani izlaz datoteci, (NoviSpremink datoteka)
			}
			if (i == 2) {
				fputs(linija, fo);//pise string sprecifiranoj datoteci ali ne ukljucuje null karakter
			}
			if (i == 3) {
				strncpy(opcija1, linija, 30);//odgovor, strncpy kopira 30 charova iz stringa linija u opcija1
			}
			if (i == 4) {
				strncpy(opcija2, linija, 30);//odgovor, kopira 30 charova iz stringa linija u opcija2
			}
			if (i == 5) {
				strncpy(opcija3, linija, 30);//odgovor,kopira 30 charova iz stringa linija u opcija3
			}
			if (i == 6) {
				strncpy(opcija4, linija, 30);//odgovor,kopira 30 charova iz stringa linija u opcija4
			}
			if (i > 1 && i <= 7) { //prazna linija
				printf("%s", linija);
			}
			if (i == 8) {
				strncpy(tocno, linija, 30); //sprema sa 8. indeksa tocan odgovor i sprema ga u polje char tocno[]
			}
			if (i == 9) {
				strncpy(kategorija, linija, 30); //9. linija je uvijek kategorija i spremam je u polje char kategorija[]
			}
		}
		pitanja(opcija, opcija1, opcija2, opcija3, opcija4, tocno, kategorija, fo,&score);//pozivam funkciju s pitanjima i proslijedjujem joj argumente.


		if (j != 9) {//j=0,1,2...8 , int polje[30]= 1,11,21,31,41,51
			if (polje[temp] + 10 < polje[3 * (j + 1)]) { //polje[5]+10<polje[3*(0+1)] ->  51<21
				for (i = polje[temp] + 10; i < polje[3 * (j + 1)]; i++) { //nasumicnih 11 redaka (predstavljaju kompletno pitanje+odgovre+tocan odgovor+kategoriju
					fgets(spremnik[k], sizeof(spremnik[k]), fp); //spremam  ih kao string u char spremnik[256][256]
					k++;
				}
			}
		}
		system("cls");

	}
	num = score;
	fprintf(fo, "\nOsvojili ste ukupnih %d kn od mogucih 1 000 000 kuna.", score);//salje formatirani izlaz datoteci, (NoviSpremink datoteka)
	fclose(fp);
	fclose(fo);
	fo = fopen("NoviSpremnik", "r");
	if (fo == NULL) {
		printf("NoviSpremink.txt linija 120");
		exit(1);
	}
	while (fgets(linija, sizeof(linija), fo)) {
		printf(" %s", linija); //ispisujem sve iz polja linija u kojem su podatci iz datoteke NoviSpremink
	}
	fclose(fo);
	retry(num);// funkcija koja ispisuje broj osvojenih novaca, pita za ponovno igranje, ukoliko ne, pita za igracevo ime(zbog pozvane point_to_point funkcije unutar funkcije retry). Ako da, ponovno pokrece kviz.
	
	return num; //vracam num koji prestavlja score varijablu 
	
	
}
void pitanja(char opcija[200], const char opcija1[200], const char opcija2[200], const char opcija3[200], const char opcija4[200], const char tocno[200], const char kategorija[200], FILE* fp,int *score) {
	FILE* ft;
	ft = fopen("tablica.bin", "ab");
	if (ft == NULL) {
		printf("Nema tablice.txt linija 133");
		exit(1);
	}
	if (score == NULL) {
		printf("Skor nije prosao 137 linija");
		exit(1);
	}
	printf("Vas odgovor:"); //unosimo jedan od ponudjenih odgovora
	scanf("%str", &opcija[0]);//na prvi indeks unosimo svoj odbair %str upisuje string
	opcija[1] = '\0'; //na opcija[200], indeks 1 (2. mjesto) stavljam null karakter jer zelim samo jednog chara koji ce predstavljati odgovor
	_strupr(opcija);//_strupt je funkcija iz UNIXA i ona cijeli string prebacuje u velika slova tako da nam odgovor moze biti napisan i velikim slovom 
	char tocanodg[200];
	char netocanodg[200];
	fprintf(fp, "%s \n", kategorija);//saljemo u datoteku Univerzalni.txt formatirani string
	if (opcija[0] == tocno[0]) { //ako je uneseno slovo koje predstavlja odgovor jednako onom koje je spremljeno u polje tocno[] u funkciji unos() onda usporedujemo u nastavku ostatak polja i prethodne funkcije i saljem ih u NoviSpremnik datoteku i kasnije ispisujem kao tocan odgovor
		if (opcija[0] == opcija1[0]) {
			fprintf(fp, "\nUnijeli ste tocan odgovor: %s \n", opcija1); //ovi if-ovi rade samo kod tocnih odgovora
		}
		if (opcija[0] == opcija2[0]) {
			fprintf(fp, "\nUnijeli ste tocan odgovor: %s \n", opcija2);
		}
		if (opcija[0] == opcija3[0]) {
			fprintf(fp, "\nUnijeli ste tocan odgovor: %s \n", opcija3);
		}
		if (opcija[0] == opcija4[0]) {
			fprintf(fp, "\nUnijeli ste tocan odgovor: %s \n", opcija4);

		}
		*score += 100000; // za svako tocno odgovoreno pitanje, dodjeljujem 100 000kn
		
		
		fprintf(fp, "\n Osvojili ste 100 000kn za ovaj tocan odgovor!!! \n");
	}
	else {
		//svaki tocan odgovor ce biti spremljen u polje tocanodg[]
		if (tocno[0] == opcija1[0]) { //usporedujemo iz proslijedjenog polja tocno[] s opcijom1 kako bi ispitali je li odgovor tocan
			strncpy(tocanodg, opcija1, 24); //kopiramo string u tocanodg[] iz opcija1 polja
		}
		if (tocno[0] == opcija2[0]) {
			strncpy(tocanodg, opcija2, 24);
		}
		if (tocno[0] == opcija3[0]) {
			strncpy(tocanodg, opcija3, 24);
		}
		if (tocno[0] == opcija4[0]) {
			strncpy(tocanodg, opcija4, 24);
		}
		tocanodg[24] = '\0';//na kraj stavljamo null char kako bi oznacio kraj 
		if (opcija[0] == opcija1[0]) { //ako odgovor nije it polja tocno[], smatra se netocnim te ga kopiramo u netocanodg[]
			strncpy(netocanodg, opcija1, 24);
		}
		if (opcija[0] == opcija2[0]) {
			strncpy(netocanodg, opcija2, 24);
		}
		if (opcija[0] == opcija3[0]) {
			strncpy(netocanodg, opcija3, 24);
		}
		if (opcija[0] == opcija4[0]) {
			strncpy(netocanodg, opcija4, 24);
		}
		netocanodg[24] = '\0';//na kraj stavljamo null char kako bi oznacio kraj 
		fprintf(fp, "Vas odgovor: %s \n", netocanodg);//u slucaju netocnog odgovora ispisujem koi odgovor odabran
		fprintf(fp, "Tocan odgovor: %s \n", tocanodg);// odmah ispod ispisujem tocan odgovor , sve u NoviSpremnik.txt
		fprintf(fp, "\n Osvojili ste 0 kuna za ovaj odgovor\n");
	}
	fprintf(fp, "-----------------------------------------------------\n");
	fprintf(fp, "-----------------------------------------------------\n");
	

}

void izlaz(void)
{
	char yes[] = "da";
	char no[] = "ne";
	char choose[3] = { 0 };
	printf("Jeste li sigurni da zelite izaci iz kviza?\n->Da\n->Ne\n\n");
	scanf("%s", &choose);
	if (_strcmpi(yes, choose) == 0)           //strcmpi usporeduje stringove                            
	{
		exit(1);
	}
	if (_strcmpi(no, choose) == 0)                                       
	{
		system("cls");
		menu();
	}
	else
	{
		system("cls");
		printf("\n('da'/'ne')\n");                         
		izlaz();
	}
}


void point_to_point(int num){       //upisuje score u tablicu

	int n = 0,m = 0;
	FILE* ft = NULL;
	ft = fopen("tablica.bin", "rb");                    
	if (ft == NULL)
	{
		fflush(ft);
		ft = fopen("tablica.bin", "wb");                //Ako ne postoji tablica.bin program ju napravi, u suprotnom ako vec postoji tablica.bin program ide dalje
		
		fwrite(&n, sizeof(int), 1, ft);
	}
	fclose(ft);
	IGRAC* igrac = NULL;
	igrac = (IGRAC*)calloc(1, sizeof(IGRAC));
	if (igrac == NULL) {
		printf("alokacija linija 245");
		exit(1);
	}
	FILE* fp = NULL;
	fp = fopen("tablica.bin", "rb+");
	if (fp == NULL)
	{
		printf("Tablica rb+ linija 252");
		exit(1);
	}
	else
	{
		fread(&m, sizeof(int), 1, fp);
		m++;
		fseek(fp, 0, SEEK_SET);
		fwrite(&m, sizeof(int), 1, fp);
		fseek(fp, 0, SEEK_END);
		printf("Osvojen iznos: %d\n", num);
		printf("Ime igraca: ");
		scanf(" %[^\n]%*c", igrac->ime); //Obicni %s nije mogao ici jer bi unijeo string do prvog razmaka,preko %[^\n]%*c se moze unijeti string pogodan za ovu situaciju
		igrac->br = num;
		fwrite(igrac, sizeof(IGRAC), 1, fp);
		system("cls");
	}
	fclose(fp);
}


void retry(int num){//funkcija koja nam opciju da ponovno pokrenemo kviz ako nismo zadovoljni rezultatom

	char yes[] = "da";
	char no[] = "ne";
	char choose[3] = { 0 };
	printf("\nOsvojena svota: %d kn\n", num);
	printf("Pokusaj ponovno?\n->Da\n->Ne\n\n");                   
	scanf("%s", &choose);
	if (_strcmpi(yes, choose) == 0)                           
	{
		unos();
	}
	if (_strcmpi(no, choose) == 0)                          
	{
		point_to_point(num); //ne opcija izaziva funkciju za upis imena i bodova 
	}
	else
	{
		system("cls");
		printf("\n('da'/'ne')\n\n");             
		retry(num);
	}
}


void tablica_rezultata(void){ //lista rezultata koju cemo sortirati i pretrazivati
   int m = 0, n = 0;
	system("cls");
	FILE* fp = NULL;
	fp = fopen("tablica.bin", "rb+");
	if (fp == NULL)
	{
		printf("Nema odigranih kvizova"); //vraca nas na menu ukoliko nema odigranih kvizova
		exit(1);
	}
	else
	{
		
		fread(&m, sizeof(int), 1, fp); //ucitava podatke iz filea u polje koje je dano preko pokazivaca
		IGRAC* igrac = NULL;
		igrac = (IGRAC*)calloc(m, sizeof(IGRAC)); //alociramo memoriju
		if (igrac == NULL) {
			printf("Linija 314, alokacija");
			exit(1);
		}
		fread(igrac, sizeof(IGRAC), m, fp);//u polje igrac ucitavamo podatke iz datoteke
		
		do{
		
			while ((getchar()) != '\n');
			player_number();
			printf("Sortiranje i pretrazivanje tablice rezultata:\n(1)Pronadji igraca\n(2)Po imenu\n(3)Po bodovima\n(4)Povratak na glavni menu\n\n\n");
			scanf("%d", &n);
			switch (n){
			
			case 1:
				system("cls");
				seek_player();
				break;
			case 2:
				system("cls");
				sort_name(igrac, m); //sortiramo po imenu 
				output_name(igrac, m);
				backup();
				break;
			case 3:
				system("cls");
				sort_score(igrac, m);//sortiramo po osvojenom iznosu
				output_score(igrac, m);
				backup();
				break;
			case 4:
				system("cls");
				menu();//vraca nas na menu
			default:
				system("cls");
				printf("Unesite jednu od danih opcija\n\n");
			}
		} while (1);
	}
}


void sort_score(IGRAC* igrac, const int n){ //selection sort LV9
	if (igrac == NULL) {
		printf("Error linija 357");
		exit(1);
	}
	int min = 0;
	for (int i = 0; i < n - 1; i++)
	{
		min = i;
		for (int j = i + 1; j < n; j++)
		{
			if ((igrac + j)->br > (igrac + min)->br)
			{
				min = j;
			}
		}
		swap((igrac + i), (igrac + min));
	}
}


void sort_name(IGRAC* igrac, const int n) //selection sort LV9
{
	if (igrac == NULL) {
		printf("Error linija 397");
		exit(1);
	}
	int min = 0;
	for (int i = 0; i < n - 1; i++)
	{
		min = i;
		for (int j = i + 1; j < n; j++)
		{
			if (strcmp((igrac + j)->ime, (igrac + min)->ime) < 0)
			{
				min = j;
			}
		}
		swap((igrac + i), (igrac + min));
	}
}


void swap(IGRAC* veci, IGRAC* manji) //LV9
{
	IGRAC temp;
	if (veci == NULL || manji==NULL) {
		printf("Error linija 402");
		exit(1);
	}
	temp = *manji;
	*manji = *veci;
	*veci = temp;
}


void output_name(IGRAC* igrac, const int n){//ispis po imenu
	int i;
	if (igrac == NULL) {
		printf("Error linija 414");
		exit(1);
	}
	
	for (i = 0; i < n; i++)
	{
		printf("\n%d. Ime: %s   Bodovi: %d\n", (i + 1), (igrac + i)->ime, (igrac + i)->br);
	}
}


void output_score(IGRAC* igrac, const int n){//ispis po osvojenom iznosu
	int i;
	if (igrac == NULL) {
		printf("output_score fija linija 428");
		exit(1);
	}
	
	for (i = 0; i < n; i++)
	{
		printf("\n%d. Ime: %s   Osvojen iznos: %d kn\n", (i + 1), (igrac + i)->ime, (igrac + i)->br);
	}
}


void backup(void)//vracamo se na menu  ovom funkcijom, primjer rekurzije
{
	while ((getchar()) != '\n');
	char s;
	printf("\nPritisnite enter za povratak u menu  ");
	s = fgetc(stdin);
	if (s == '\n')
	{
		system("cls");
		menu();
	}
	else
	{
		system("cls");
		printf("\nPogresan unos, pokusajte ponovno\n\n");
		backup();
	}
}


void player_number(void)
{
	int brojkvizova;// broj odigranih kvizova se moze poistovijetiti s brojem igraca zato sto se igracevo ime trazi nakon odigranog kviza
	FILE* fp;
	fp = fopen("tablica.bin", "rb+");
	rewind(fp);//postavlja file poziciju na pocetak
	if (fp == NULL)
	{
		printf("Error linija 467 tablica");
		exit(1);
	}
	else
	{
		fseek(fp, 0, SEEK_END);//stavljamo kursor na kraj filea kako bi uzeli njegovu cijelu velicinu u+i u sljedecm redu je pronsali
		brojkvizova = ftell(fp) / sizeof(IGRAC);//cita broj zapisanih igraca u strukturi,ftell vraca trenutnu poziciju u fileu
		printf("Broj odigranih kvizova: %d\n\n", brojkvizova);
	}
	fclose(fp);
}


void seek_player(void) //funkcija koja pretrazuje igraca
{   int brojac = 0;
	FILE* fp = NULL;
	fp = fopen("tablica.bin", "rb+");
	if (fp == NULL)
	{
		printf("Error linija 486 tablica");
		exit(1);
	}
	fseek(fp, 0, SEEK_END);  //od 0ta pozicija od kraja, tamo je "kursor"
	fseek(fp, sizeof(int), SEEK_SET); //postavlja se na pocetak filea 
	char polje[20] = { 0 };
	printf("Unesite trazeno ime: ");
	scanf(" %[^\n]%*c", polje);
	
	while (fread(&igrac, sizeof(igrac), 1, fp) == 1)
	{
		if (_strcmpi(igrac.ime, (polje)) == 0) //_strcmpi usporeduje uneseno ime s imenima u strukturi(nije case sensitive nalazi se u string.h)
		{
			printf("\nIme: %s\nBroj bodova: %d\n", igrac.ime, igrac.br);
			brojac++;                                                                            
		}
	}
	if (brojac == 0)       //ukoliko je brojac 0 onda nema trazenog imena                                                                    
	{
		printf("Ime nije pronadeno");
	}
	retry_search(); //ponovno trazenje ukoliko je pozeljno
	fclose(fp);
}


void retry_search(void) //primjer rekurzivne funkcije LV10
{
	char yes[] = "da";
	char no[] = "ne";
	char choose[3] = { 0 };
	printf("\n\nZelite li ponovo pretraziti igraca?\n->Da\n->Ne\n");
	scanf("%s", &choose);
	if (_strcmpi(yes, choose) == 0) //strcmpi vraca 0 ako su uneseni stringovi jednaki,ako je string1<string2 onda vraca negativnu vrijdnost strong1>string2 vraca pozitivnu vrijednost
	{
		system("cls");
		seek_player(); //pozitivan slucaj, trazimo igraca
	}
	if (_strcmpi(no, choose) == 0)
	{
		system("cls");
		menu();   //negativan slucaj, vracamo se u glavni menu
	}
	else
	{
		system("cls");
		printf("('da'/'ne')\n\n");
		retry_search();   //u suprotnom, ponovno pozivamo funkciju kako bi korisnik unio da ili ne kao opciju
	}
}
void info(void)
{
	system("cls");
	printf("\nDobrodosli u netipicni kviz popularnog Milijunasa!\nSamo je jedan tocan odgovor na svako pitanje i donosi 100 000kn.\nUkoliko odgovorite krivo, osvajate 0kn,ali smijete nastaviti s kvizom do kraja.\nKako bi unijeli svoj odgovor upisite jedno od A B C D (radi i s malim slovima) potom za unos pritisnite enter.\nSvoje rezultate i rezultate drugih igraca mogu se vidjeti odabirom opcije -Rezultati-\nSretno! :)\n\n");
	backup();//vracam se na menu
}